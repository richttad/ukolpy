#brownuv pohyb
from time import sleep
from os import get_terminal_size as gts
from random import randint
#from termcolor import colored
import sys
import numpy

print('\033[2J\033[?25l')
(maxy,maxx)=gts()
x=randint(int(maxx/4),int(maxx/2 +maxx/4))
y=randint(int(maxy/4),int(maxy/2 +maxy/4))
t=numpy.zeros((maxx+1, maxy+1))
skore = 0

try:
  while True:
    if x == 1 or y == 1:
      break
    if y == maxy-1:
      break
    if x == maxx-1:
      break

    print('\033[' + str(x) + ';' + str(y) + 'H', end='')
    print('\x1b['+str(int(40+t[x+1][y+1]))+'m ', end='', flush=True)
    t[x+1][y+1]+=1
    i=randint(0,5)
    if i==4: 
      x+=1
      if x>maxx: 
        x-=2
    if i==1:
      x-=1
      if x<=0: 
        x+=2
    if i==2:
      y+=1
      if y>maxy: 
        y-=2
    if i==3:
      y-=1
      if y<=0: 
        y+=2
    sleep(0.01)
    skore += 1
except KeyboardInterrupt:
  pass
print('\x1b[0m\033[?25h', end='')
print(skore)
