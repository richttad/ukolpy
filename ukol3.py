import math
#with open ('2-P6.pnm', 'bw') as f:
#  f.write(b'P6\n256 256\n255\n')
#  for i in range(256):
#    for j in range(256):
#      a=int(math.sqrt(i*i+j*j))
#      if a>255:a=255
#      b=int(255-math.sqrt(i*i+j*j))
#      if b<255:b=0
#      f.write(bytes([a,0,0]))


with open ('2-P6.pnm', 'bw') as f:
  f.write(b'P6\n768 768\n255\n')
  for i in range(768):
  	for j in range(768):
  		x = (i-384) 
  		y = (j-384)
  		r = 0 
  		g = 0
  		b = 0
  		if ((((x**2)+(y**2) )< 2000)and (((x**2)+(y**2))> 200)):
  			a = math.degrees(math.atan2(x,y) + math.pi)
  			a = a / 60
  			if a < 1:
  				r = 255 - int(a*128)
  				g = int(a*128)
  			elif a<2:
  				r = 128 -int((a-1)*128)
  				g = 128 + int((a-1)*128)
  			elif a < 3:
  				g = 255 - int((a-2)*128)
  				b = int((a-2)*128)
  			elif a <4 :
  				g = 128 -int((a-3)*128)
  				b = 128 + int((a-3)*128)
  			elif a < 5:
  				b = 255 - int((a-4)*128)
  				r = int((a-4)*128)
  			elif a < 6:
  				b = 128 -int((a-5)*128)
  				r = 128 + int((a-5)*128)
  			if r>255 or g >255 or b>255:
  				print(r,g,b)
  			f.write(bytes([r,g,b]))
  		else:
  			f.write(bytes([0,0,0]))
"""with open ('2-P6.pnm', 'bw') as f:
  f.write(b'P6\n768 768\n255\n')
  for i in range(768):
  	for j in range(768):
  		x = (i-384) 
  		y = (j-384)
  		l = 0 
  		s = 0
  		d = 0
  		if ((((x**2)+(y**2) )< 400)and (((x**2)+(y**2))> 40)):
  			a = math.degrees(math.atan2(x,y) + math.pi)
  			a = a / 120

  			if a < 1:
  				l = int(a*128)
  				d = int((a+1)*128)
  			elif a<2:
  				l = int(a*128)
  				s = int((a-1)*128)
  			elif a < 3:
  				d = int((a-2)*128)
  				s = int((a-1)*128)
  			f.write(bytes([l,s,d]))
  		else:
  			f.write(bytes([0,0,0]))
"""


